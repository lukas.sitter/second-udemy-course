/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-alert */
import React from 'react';
import StripeCheckout from 'react-stripe-checkout';

const StripeCheckoutButton = ({ price }) => {
  const priceForStripe = price * 100;
  const publishableKey = 'pk_test_51HpE8RDKbcq9pkFcXR0BloF8wA4O9uGHMtohn5lvnsUbvPiVkN4ko13au6PMBjDGXmPGQ3KT4nEFc6R21PblRQKI00D8f87kQl';

  const onToken = (token) => {
    console.log(token);
    alert('Payment Successful');
  };

  return (
    <StripeCheckout
      label="Pay Now"
      name="Best e-shop"
      billingAddress
      shippingAddress
      image="https://svgshare.com/i/CUz.svg"
      description={`Your total is $${price}`}
      amount={priceForStripe}
      panelLabel="Pay now"
      token={onToken}
      stripeKey={publishableKey}
    />
  );
};

export default StripeCheckoutButton;
