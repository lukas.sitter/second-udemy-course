import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const HeaderContainer = styled.div`
    height: 70px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-bottom: 25px;
`;

export const LogoContainer = styled(Link)`
    height: 100%;
    width: 200px;
    padding: 15px 10px;
    display: flex;
    align-items: center;
      
    &:hover {
        transform: scale(1.15);
        transition: ease-in-out 0.2s;
      }
`;

export const Name = styled.div`
    margin-left: 25px;
    font-weight: 600;
    font-size: 24px;
`;

export const OptionsContainer = styled.div`
    width: 50%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

export const OptionLink = styled(Link)`
    padding: 15px 10px;
    cursor: pointer;
    font-weight: 600;

    &:hover {
        transform: scale(1.15);
        transition: ease-in-out 0.2s;
      }
`;
