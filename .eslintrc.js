module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/prop-types': 'off',
    'jsx-filename-extension': 'off',
    'linebreak-style': 'off',
    'no-undef': 'off',
    'max-len': 'off',
    'no-plusplus': 'off',
    'no-use-before-define': 'off',
    'react/jsx-no-undef': 'off',
    'react/jsx-filename-extension': 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': 'off',
    'react/react-in-jsx-scope': 'off',
    'import/order': 'off',
    'react/no-unused-state': 'off',
    'react/destructuring-assignment': 'off',
    'no-unused-vars': 'off',
    'no-shadow': 'off',
    'no-useless-return': 'off',
  },
};
